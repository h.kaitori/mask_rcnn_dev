# MaskRCNN

## Model

```
Z:\kaitori\MaskRCNN\model
```

## 学習

```
python3 train.py train --dataset=./dataset --weights=coco
```

## 推論

```
python3 main.py
```

## オイラー設定

EULER_EMAIL= 'xxxx'

EULER_PASSWORD = 'xxxx'
